# Future plans

## Purpose
The following is an unordered list of possible future plans for Quentin (**Q**) and Denisa (**D**), with advantages, disadvantages and dependencies.

The [first section](#startupvscorporation) discusses advantages of working for our own startup in comparison with working for an existing corporation.

The [second section](#startupplans) explores plans for doing a startup in various countries and situations.

Similarly, the [third section](#corporateworkplans) details possible plans for corporate jobs.

Finally, a summary table is provided in the [last section](#summarytable).

## I - Startup vs Corporation

### Advantages of startup
- Use a large portion of current skillset
- Learn new skills in all areas
- Obtain a future corporate job easier
    - by accomplishing something concrete with your startup
    - by getting famous with your startup
- In case of **failure**
    - Obtaining funding for several months is the same as fixed income
    - Experience and skills are not lost
- In case of **success**
    - In 4 years, profit `$10M+` instead of ~ `$2M` in a corporate job
    - Work on something you like and bring something to the world
    - *Only* way for D and Q to move to the US together

### Disdvantages of startup
- More work and stressful

### Roles
- Q is in charge of
    - Software development
        - Backend
        - Android
    - Design
- D is in charge of
    - Software development
        - iOS
    - Networking and marketing
    - Design

## II - Startup plans

### Canada then US
- Challenging but with the greatest potential
- Requires funding from Canadian incubators/VCs to obtain a startup work permit
- Deadline is **summer 2018**
- Q and D would obtain a Visa to work on the startup in Canada
    - Q and D both have Masters (specialized individuals)
    - Q and D both speak English and French more or less
- Q and D want to go to Canada
    - People are nice (on the East side at least)
    - Montreal is lovely and cheap
- It is relatively easy to bring the startup to the US later

### Bucharest / Luxembourg then Canada then US
Consists in working from home to finish the product. 

This is only applicable if:

- The product is promising
- The product is not ready in June 2018
- From June 2018 to August 2018

If the product is still not ready in August 2018, get a [corporate job](#corporateworkplans) in August 2018 and keep on working on the product during free time. See the section [**Canada then US**](#canadathenus) above.

### US (seems bad)
- Nearly impossible to startup in the US without a greencard
- Q could use his OPT work status to work 1 year **max** on the startup
- D could not work legally on the startup

### London
- Easier although work permit will be required
- Good influential pole, but likely limited to Western Europe
- Possibility to move the startup to the US later

### Paris/ Luxembourg / Romania
See [the summary table](#summarytable)

## III - Corporate work plans

### Canada (Montreal)
- Easier to obtain a work permit than in the US
- Cheaper than US
- Less corporate growth potential than in the US
- Startup work during free time

### US
- Best corporate growth potential
- Q can work **3 years** with OPT permit and then _H1B_
- D needs _H1B_, which is _nearly impossible_
- Potential plan
    - Q works 3 - 6 months for a AAA company (i.e. Google) to add an extra certification for a future startup with D
    - Q works on startup product during free time
    - D works on startup product (as a personal project) full time, living with Q
    - Move to Canada / other when startup product is ready

### Paris / London / Luxembourg / Romania
- Closer to family
- Less potential that Canada / US
- See the [summary table](#summarytable) below

# IV - Summary table

- The higher the number the better.
- Scores are calculated as **Work permit Q** + **Work permit D** + **Potential** x 3 + **Work required** x 1.3 + **Living cost** x 1.6 + **Move to US** x 1.7

| Plan               | Work permit Q | Work permit D | Potential | Work required | Living cost | Move to US | Score |
| ------------------ | ------------- | ------------- | --------- | ------------- | ----------- | ---------- | ----- |
| Startup Canada     | 2             | 2             | 4         | 1             | 3           | 3          | 27.2  |
| Startup US         | 1             | 1             | 5         | 1             | 1           | 4          | 26.7  |
| Startup Paris      | 4             | 4             | 3         | 2             | 2           | 1          | 24.5  |
| Startup London     | 3             | 3             | 4         | 2             | 2           | 2          | 27.2  |
| Startup Romania    | 4             | 4             | 1         | 3             | 4           | 1          | 23    |
| Startup Luxembourg | 4             | 4             | 2         | 3             | 3           | 1          | 24.4  |
| Work Canada        | 3             | 3             | 2         | 4             | 3           | 3          | 27.1  |
| Work US            | 4             | 1             | 3         | 3             | 1           | 3          | 24.6  |
| Work Romania       | 4             | 4             | 1         | 4             | 4           | 1          | 24.3  |
| Work Luxembourg    | 4             | 4             | 1         | 4             | 3           | 1          | 22.7  |
| Work Paris         | 4             | 4             | 1         | 4             | 2           | 1          | 21.1  |
| Work London        | 4             | 4             | 2         | 3             | 2           | 2          | 24.5  |
